import React from 'react';
import './Card.css'

const Card = props => {
    let suit = null;
    let suitIcon = null;
    switch (props.suit.toLowerCase()) {
        case 'd':
            suit = 'diams';
            suitIcon = '♦'
            break;
        case 'h':
            suit = 'hearts';
            suitIcon = '♥'
            break;
        case 'c':
            suit = 'clubs';
            suitIcon = '♣';
            break
        case 's':
            suit = 'spades';
            suitIcon = '♠';
            break;
        default:
            suit = null;
    }
    return (
        <div className="playingCards">
            <div className={`card ${suit} rank-${props.rank.toLowerCase()}`}>
                <span className="rank">{props.rank}</span>
                <span className="suit">{suitIcon}</span>
            </div>
        </div>
    );
};

export default Card;