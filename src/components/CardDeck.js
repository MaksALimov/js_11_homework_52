function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
}

class CardDeck  {
    constructor() {
        this.cards = []

        const suits = ['H', 'C', 'D', 'S'];
        const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

        suits.forEach(suit => {
            ranks.forEach(rank => {
                this.cards.push(Object.assign({suit: suit, rank: rank}));
            });
        });
    };

    getCard = () => {
        const randomCard = getRandomInt(0, this.cards.length);
        const card = this.cards[randomCard];
        this.cards.splice(randomCard, 1);
        return card;
    };

    getCards = (howMany) => {
        let randomCards = [];
        for (let i = 0; i < howMany; i++) {
            randomCards.push(this.getCard());
        }
        return randomCards;
    };
}

export default CardDeck;